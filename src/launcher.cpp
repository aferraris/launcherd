#include <QtGlobal>
#include <QDBusConnection>

#include "launcher.h"
#include "launcher_adaptor.h"
#include "launcher_interface.h"

Launcher::Launcher(int &argc, char **argv) : QApplication(argc, argv)
{
    m_processes = new QList<QProcess>();
    m_dbus_processes = new QList<QString>();
}

Launcher::~Launcher()
{
}

void Launcher::init(void)
{
    new LauncherAdaptor(this);
    QDBusConnection dbusconnection = QDBusConnection::sessionBus();
    dbusconnection.registerObject("/org/automotivelinux/Launcher", this);
    dbusconnection.registerService("org.automotivelinux.Launcher");
}

void Launcher::activate(const QString &appid, const QString &command, bool dbus)
{
    qInfo() << "Activating app" << appid;
}
