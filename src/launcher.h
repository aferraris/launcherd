#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QApplication>
#include <QProcess>
#include <QString>
#include <cstdlib>

class Launcher : public QApplication
{
    Q_OBJECT

public:
    explicit Launcher(int &argc, char **argv);
    ~Launcher();

    void init(void);

signals:
    void activated(QString app_id);

public slots:
    void activate(const QString &app_id, const QString &command, bool dbus);

private:
    QList<QProcess> *m_processes;
    QList<QString> *m_dbus_processes;
};

#endif
